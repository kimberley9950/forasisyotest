FROM php:8.3-fpm

RUN apt-get update \
    && apt-get install -y libzip-dev zip unzip git \
    && docker-php-ext-install pdo_mysql zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www

COPY . .

RUN composer install

RUN php artisan key:generate

CMD php artisan serve --host=0.0.0.0 --port=8000
