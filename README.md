## Quick Run

1. cp .env.example .env
2. composer install
3. docker-compose up -d
4. Request [URI](http://localhost:8000/api/currencyExchange?source=USD&target=JPY&amount=1,525)
5. docker-compose exec web php artisan test
