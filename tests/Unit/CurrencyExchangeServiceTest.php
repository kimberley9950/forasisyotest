<?php

namespace Tests\Unit;

use App\Exceptions\CurrencyTypeException;
use App\Services\CurrencyExchangeService;
use Tests\TestCase;

class CurrencyExchangeServiceTest extends TestCase
{
    private CurrencyExchangeService $service;

    private array $currencies;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CurrencyExchangeService::class);

        $this->currencies = $this->service->currencies['currencies'];
    }

    public function testExchangeSuccess(): void
    {
        $source = 'USD';
        $target = 'JPY';
        $targetRate = $this->currencies[$source][$target];

        # Case1:千位符
        $result = $this->service->exchangeCurrency(
            $source,
            $target,
            $amount = '1,525',
        );

        # 計算需拿掉千位符
        $amount = str_replace(',', '', $amount);

        $this->assertEquals(
            number_format($amount * $targetRate, 2),
            $result
        );

        # Case2:整數
        $result = $this->service->exchangeCurrency(
            $source,
            $target,
            $amount = 1525,
        );

        $this->assertEquals(
            number_format($amount * $targetRate, 2),
            $result
        );


        # Case3:字串帶有小數
        $result = $this->service->exchangeCurrency(
            $source,
            $target,
            $amount = '1526.632222',
        );

        # 小數需四捨五入至第二位
        $amount = round($amount, 2);

        $this->assertEquals(
            number_format($amount * $targetRate, 2),
            $result
        );

        # Case4:帶有千位符的小數
        $result = $this->service->exchangeCurrency(
            $source,
            $target,
            $amount = '1,526.64345',
        );

        # 計算需拿掉千位符
        $amount = str_replace(',', '', $amount);

        # 小數需四捨五入至第二位
        $amount = round($amount, 2);

        $this->assertEquals(
            number_format($amount * $targetRate, 2),
            $result
        );

        # Case5:浮點數
        $result = $this->service->exchangeCurrency(
            $source,
            $target,
            $amount = 1526.63425,
        );

        # 小數需四捨五入至第二位
        $amount = round($amount, 2);

        $this->assertEquals(
            number_format($amount * $targetRate, 2),
            $result
        );

        # Case6:帶有千位符的浮點數
        $result = $this->service->exchangeCurrency(
            $source,
            $target,
            $amount = "1,526.62335",
        );

        # 計算需拿掉千位符
        $amount = str_replace(',', '', $amount);

        # 小數需四捨五入至第二位
        $amount = round($amount, 2);

        $this->assertEquals(
            number_format($amount * $targetRate, 2),
            $result
        );
    }

    public function testSourceAndTargetNotSupport(): void
    {
        $source = 'DKWDQWLD';
        $target = 'JPY';
        $amount = '1,525';
        $currenciesTypes = array_keys($this->currencies);
        $currenciesList = implode(', ', $currenciesTypes);

        $this->expectException(CurrencyTypeException::class);
        $this->expectExceptionMessage("source 格式錯誤，僅$currenciesList");

        $this->service->exchangeCurrency(
            $source,
            $target,
            $amount,
        );

        $source = 'USD';
        $target = 'DWLWDL';

        $this->expectException(CurrencyTypeException::class);
        $this->expectExceptionMessage("target 格式錯誤，僅$currenciesList");

        $this->service->exchangeCurrency(
            $source,
            $target,
            $amount,
        );
    }

    public function testAmountUnrecognized()
    {
        $source = 'USD';
        $target = 'JPY';
        $amount = '1,1,1,1,1';

        $this->expectException(CurrencyTypeException::class);
        $this->expectExceptionMessage('amount 千位符格式有誤');

        $this->service->exchangeCurrency(
            $source,
            $target,
            $amount,
        );

        $amount = '1@SS@(#D)@';

        $this->expectException(CurrencyTypeException::class);
        $this->expectExceptionMessage('amount 需皆為數字');

        $this->service->exchangeCurrency(
            $source,
            $target,
            $amount,
        );
    }
}
