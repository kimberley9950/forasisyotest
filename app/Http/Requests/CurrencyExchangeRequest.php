<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CurrencyExchangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $currencies = array_keys(config('exchangeRate')['currencies']);

        return [
            'source' => [
                'required',
                'string',
                Rule::in($currencies),
            ],
            'target' => [
                'required',
                'string',
                Rule::in($currencies),
            ],
            'amount' => [
                'required',
                fn ($attr, $val, $fail) => is_string($val) || is_numeric($val)
            ],
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'source' => strtoupper($this->source),
            'target' => strtoupper($this->target),
        ]);
    }
}
