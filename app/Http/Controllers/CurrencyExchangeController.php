<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrencyExchangeRequest;
use App\Services\CurrencyExchangeService;
use Illuminate\Http\JsonResponse;

class CurrencyExchangeController extends Controller
{
    public function __construct(
        private CurrencyExchangeService $service,
    ) {
    }

    /**
     * 取得幣別兌換
     *
     * @param CurrencyExchangeRequest $request
     * 
     * @return JsonResponse
     */
    public function getExchange(CurrencyExchangeRequest $request): JsonResponse
    {
        return $this->jsonResponse([
            'msg' => 'success',
            'amount' => $this->service->exchangeCurrency(...$request->validated()),
        ]);
    }
}
