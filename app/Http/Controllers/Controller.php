<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;

abstract class Controller
{
    public function jsonResponse(
        array $data = [],
        $status = Response::HTTP_OK,
        $header = [],
        $flag = 0
    ) {
        return response()->json(...func_get_args());
    }
}
