<?php

namespace App\Services;

use App\Exceptions\CurrencyTypeException;

class CurrencyExchangeService
{
    /**
     * CurrencyService constructor.
     *
     * @param array $currencies by CurrencyExchangeServiceProvider to inject.
     */
    public function __construct(public array $currencies)
    {
    }

    # amount有float，是看到測試案例有寫輸入的數字有小數的情形
    public function exchangeCurrency(
        string $source,
        string $target,
        int|float|string $amount,
    ): string {
        $this->validateParams($source, $target, $amount);

        $targetRate = $this->currencies['currencies'][$source][$target];

        return number_format($amount * $targetRate, 2);
    }

    private function validateParams(
        string &$source,
        string &$target,
        int|float|string &$amount,
    ): void {
        # 統一大寫
        $source = strtoupper($source);
        $target = strtoupper($target);

        $currenciesTypes = array_keys($this->currencies['currencies']);
        $currenciesList = implode(', ', $currenciesTypes);

        if (!in_array($source, $currenciesTypes)) {
            throw new CurrencyTypeException("source 格式錯誤，僅$currenciesList");
        }

        if (!in_array($target, $currenciesTypes)) {
            throw new CurrencyTypeException("target 格式錯誤，僅$currenciesList");
        }

        # 驗證千位符格式
        if (str_contains($amount, ',')) {
            # 1.去除千位符
            $amountRemoveCommas = str_replace(',', '', $amount);

            # 2.確保皆為數字
            if (!is_numeric($amountRemoveCommas)) {
                throw new CurrencyTypeException('amount 需皆為數字');
            }

            # 3.含有小數，amount四捨五入到第二位
            # 移除千位符的也須跟著被四捨五入
            # 使得後續驗證千位符數字才能相同
            $roundPrec = 0;
            if (str_contains($amount, '.')) {
                $roundPrec = 2;
                $amountRemoveCommas = round($amountRemoveCommas, $roundPrec);

                $amount = number_format($amountRemoveCommas, $roundPrec);
            }

            # 確保千位符後與原字串相同
            if ($amount !== number_format($amountRemoveCommas, $roundPrec)) {
                throw new CurrencyTypeException('amount 千位符格式有誤');
            }

            $amount = $amountRemoveCommas;

            # 純小數或小數字串情況
        } elseif (is_float($amount) || is_string($amount)) {
            $amount = round($amount, 2);
        }
    }
}
