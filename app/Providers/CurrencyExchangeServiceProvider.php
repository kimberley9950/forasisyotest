<?php

namespace App\Providers;

use App\Services\CurrencyExchangeService;
use Illuminate\Support\ServiceProvider;

class CurrencyExchangeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(
            CurrencyExchangeService::class,
            fn () => new CurrencyExchangeService(config('exchangeRate'))
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
