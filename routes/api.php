<?php

use App\Http\Controllers\CurrencyExchangeController;
use Illuminate\Support\Facades\Route;

Route::get('currencyExchange', [CurrencyExchangeController::class, 'getExchange']);
